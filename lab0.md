# Lab 0 - Login to Your Environment

## Get your user number

### Please get your user number from the instructor.  Your username will be 'evals<#>' with the password of 'Password1' from this point forward. 

## Login 

Navigate to https://tutorial-web-app-webapp.apps.07c0.summit.opentlc.com/ and login with your user id and password. 

![lab0-login.png](./images/lab0-login.png)

![lab0-provisioning.png](./images/lab0-provisioning.png)

Provisioning of your environment will begin. This may take a few minutes. Once it is done you will see a screen like below.  Now you can move onto Lab 1. 

![lab0-post-provisioning.png](./images/lab0-post-provisioning.png) 

[Go To Lab 1](https://gitlab.com/redhatsummitlabs/learning-to-use-the-camel-rest-dsl-with-3scale-and-openshift/blob/master/lab1.md)
